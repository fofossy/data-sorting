using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using IDMP.Device;
using IDMP.Device.Views;
using System.Windows;
using System.Collections.ObjectModel;
using Prism.Events;
using IDMP.Device.MessageEvent;
using IDMP.DBService.Models;
using IDMP.LogService;
using IDMP.MessageEvent;
using IDMP.BaseControls;
using DotNet.Language;
using System.ComponentModel;
using KIMS.Device.Models;
using System.Threading.Tasks;
using System.Threading;

namespace IDMP.Device.ViewModels
{
    public class WritingFrequencyViewModel : BindableBase
    {
        private ObservableCollection<DeviceParamTemplate> _deviceParamTemplates = new ObservableCollection<DeviceParamTemplate>();
        public ObservableCollection<DeviceParamTemplate> DeviceParamTemplates
        {
            get { return _deviceParamTemplates; }
            set { SetProperty(ref _deviceParamTemplates, value); }
        }

        //全选
        private bool _allselect = false;
        public bool AllSelect
        {
            get { return _allselect; }
            set
            {
                SetProperty(ref _allselect, value);
            }
        }
        private bool _showNotData = false;
        public bool IsShowNotDataDetail
        {
            get { return _showNotData; }
            set
            {
                SetProperty(ref _showNotData, value);
            }
        }

        private string _filterText = string.Empty;
        public string FilterText
        {
            get { return _filterText; }
            set
            {
                SetProperty(ref _filterText, value);
            }
        }
        private double _progressValue = 0;
        public double ProgressValue
        {
            get { return _progressValue; }
            set
            {
                SetProperty(ref _progressValue, value);
            }
        }



        private void InitData()
        {
            DeviceParamTemplates.Clear();
            for (int i = 0; i < 20; i++)
            {
                DeviceParamTemplates.Add(new DeviceParamTemplate(i, $"Template-{i}", DateTime.Now.AddDays(-i)));
            }
            IsShowNotDataDetail = false;
        }
        /// <summary>
        /// 初始化、添加、编辑和搜索后按时间排序
        /// </summary>
        private void OrderByDescendingUpdateTime()
        {
            //_freqBandTestCases = new ObservableCollection<FreqBandTestCase>(_freqBandTestCases.OrderByDescending(u => Convert.ToDateTime(u.UpdateTime)));
            //int index = 1;
            //_freqBandTestCases.ToList().ForEach(u => u.RowNo = index++);
            //OnSelUser();

            //RaisePropertyChanged("FreqBandTestCases");
        }
        public DelegateCommand AddCommand { get; private set; }
        public DelegateCommand BatchDeleteCommand { get; private set; }
        public DelegateCommand SearchCommand { get; private set; }
        public DelegateCommand IsSelectCommand { get; private set; }
        public DelegateCommand IsAllSelectCommand { get; private set; }
        public DelegateCommand<string> EditCommand { get; private set; }
        public DelegateCommand<string> WriteFrequencyCommand { get; private set; }
        public DelegateCommand<string> DeleteCommand { get; private set; }
        public DelegateCommand ResetCommand { get; private set; }

        IEventAggregator _ea;
        public WritingFrequencyViewModel(IEventAggregator ea)
        {
            _ea = ea;
            InitData();
            AddCommand = new DelegateCommand(OnAddCommand);
            BatchDeleteCommand = new DelegateCommand(OnBatchDeleteCommand);
            SearchCommand = new DelegateCommand(OnSearchCommand);
            IsSelectCommand = new DelegateCommand(() => OnSelectFreqBand());
            IsAllSelectCommand = new DelegateCommand(() => OnAllSelUser());
            EditCommand = new DelegateCommand<string>((name) => OnEditCommand(name));
            WriteFrequencyCommand = new DelegateCommand<string>((name) => OnWriteFrequencyCommand(name));
            DeleteCommand = new DelegateCommand<string>((name) => OnDeleteCommand(name));
            ResetCommand = new DelegateCommand(OnResetCommand);

            _ea.GetEvent<DeviceOperEvent>().Subscribe(OnDeviceOperEvent);
        }

        private void OnDeviceOperEvent(DeviceOperEventModel userevent)
        {
        }
        private void OnSelectFreqBand()
        {
            if (DeviceParamTemplates == null || DeviceParamTemplates.Count == 0)
            {
                AllSelect = false;
                return;
            }

            if (DeviceParamTemplates.All(freq => freq.IsSelected))
            {
                AllSelect = true;
                return;
            }


            if (DeviceParamTemplates.Any(freqBand => !freqBand.IsSelected))
            {
                AllSelect = false;
                return;
            }
        }

        private void OnAllSelUser()
        {
            DeviceParamTemplates.ToList().ForEach(user =>
            {
            });
        }

        private void OnAddCommand()
        {
            FreqBandTestCase freqBand = new FreqBandTestCase();
            _ea.GetEvent<DeviceOperEvent>().Publish(new DeviceOperEventModel() { });
        }

        private void OnBatchDeleteCommand()
        {
            if (!DeviceParamTemplates.Any(freqBand => freqBand.IsSelected))
            {
                AutoCloseWindow.ShowDialog(LanguagePackage.FindKey("Tip"), LanguagePackage.FindKey("NoSelected"));
                return;
            }
            //if (MessageBoxResult.Yes == MessageBox.Show(String.Format("确定删除已选中账号？"), "Option",
            //    MessageBoxButton.YesNo))            
            if (CMessageBoxResult.Yes == CMessageBox.Show(LanguagePackage.FindKey("BatchDeleteConfigTip"), LanguagePackage.FindKey("Tip"), CMessageBoxButton.YesNO))
            {

                //MessageBox.Show(tip);
                //AutoCloseWindow.ShowDialog(LanguagePackage.FindKey("DeleteConfig"), LanguagePackage.FindKey(tipkey));
                RaisePropertyChanged("FreqBandTestCases");
            }
        }

        private void OnSearchCommand()
        {
            //过滤ShowUsers。通过FilterText 
            #region old
            //ResultInfo result = DeviceManager.Instance.GetAllConfigs();

            //if (result.IsSuccess)
            //{
            //    _showUsers = new ObservableCollection<BaseUser>();
            //    var list = result.ResultValue.Where(user => user.Name.Contains(FilterText.Trim()) || user.NickName.Contains(FilterText.Trim())).ToList();
            //    list.ForEach(user => _showUsers.Add(user));

            //    OrderByDescendingUpdateTime();

            //    RaisePropertyChanged("ShowUsers");
            //}
            #endregion

            #region new

            OrderByDescendingUpdateTime();
            RaisePropertyChanged("FreqBandTestCases");
            #endregion
        }

        private void OnResetCommand()
        {
            //过滤ShowUsers。通过FilterText 
            //ResultInfo result = DeviceManager.Instance.GetAllConfigs();
            //if (result.IsSuccess)
            //{
            //    _deviceParamTemplates = new ObservableCollection<FreqBandTestCase>();

            //    OrderByDescendingUpdateTime();

            //    RaisePropertyChanged("FreqBandTestCases");
            //}
        }


        private void OnEditCommand(string name)
        {
            //var opuser = _freqBandTestCases.FirstOrDefault(usr => usr.Name == name);
            //_ea.GetEvent<ConfigOperEvent>().Publish(new ConfigOperEventModel1() { user = opuser, isnew = false });
        }
        private void OnWriteFrequencyCommand(string name)
        {
            //if (MessageBoxResult.Yes == MessageBox.Show(String.Format("确定重置为默认密码？"), "Option",
            //    MessageBoxButton.YesNo))
            if (CMessageBoxResult.Yes == CMessageBox.Show("确定使用模板写频？", LanguagePackage.FindKey("Tip"), CMessageBoxButton.YesNO))
            {
                Task.Run(() =>
                {
                    for (int i = 1; i <= 100; i++)
                    {
                        ProgressValue = i;
                        Thread.Sleep(100);
                    }
                });
                //LogInfoHelper.UpdateLog(_ea, LogOpModule.OP_Optera, name, tipkey);
                //MessageBox.Show(tip);
                //AutoCloseWindow.ShowDialog(LanguagePackage.FindKey("ResetPwd"), LanguagePackage.FindKey(tipkey));
            }
        }

        private void OnDeleteCommand(string name)
        {
            //if (MessageBoxResult.Yes == MessageBox.Show(String.Format("确定删除该账号？"), "Option",
            //    MessageBoxButton.YesNo))
            if (CMessageBoxResult.Yes == CMessageBox.Show(String.Format("确定删除该模板？"), LanguagePackage.FindKey("Tip"), CMessageBoxButton.YesNO))
            {
            }
        }
    }
}

using Prism.Mvvm;
using System;

namespace KIMS.Device.Models
{
    public class DeviceParamTemplate : BindableBase
    {
        public Guid Guid { get; set; } = Guid.NewGuid();

        public bool _isSelected=false;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        private int _index = 1;
        public int Index
        {
            get => _index;
            set => SetProperty(ref _index, value);
        }

        private string _name = string.Empty;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private DateTime _updateTime = DateTime.Now;
        public DateTime UpdateTime
        {
            get => _updateTime;
            set => SetProperty(ref _updateTime, value);
        }

        public DeviceParamTemplate(int index, string name, DateTime updateTime)
        {
            Index = index;
            Name = name;
            UpdateTime = updateTime;
        }
    }
}
